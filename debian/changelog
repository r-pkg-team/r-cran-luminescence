r-cran-luminescence (1.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.1 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 27 Feb 2025 09:27:55 +0900

r-cran-luminescence (0.9.26-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Tue, 28 Jan 2025 15:39:40 +0900

r-cran-luminescence (0.9.23-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 10 Nov 2023 20:39:56 +0100

r-cran-luminescence (0.9.22-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 18 Aug 2023 23:11:02 +0200

r-cran-luminescence (0.9.21-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 06 Feb 2023 13:42:10 +0100

r-cran-luminescence (0.9.20-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 20 Jul 2022 06:58:49 +0200

r-cran-luminescence (0.9.19-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 13 Mar 2022 15:34:24 +0100

r-cran-luminescence (0.9.18-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 23 Jan 2022 17:34:43 +0100

r-cran-luminescence (0.9.17-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 14 Jan 2022 22:11:55 +0100

r-cran-luminescence (0.9.16-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-httr, r-cran-mclust,
      r-cran-plotrix, r-cran-raster, r-cran-readxl and r-cran-xml.

 -- Andreas Tille <tille@debian.org>  Sat, 04 Dec 2021 22:13:06 +0100

r-cran-luminescence (0.9.15-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Drop patch applied upstream

 -- Andreas Tille <tille@debian.org>  Mon, 11 Oct 2021 13:51:36 +0200

r-cran-luminescence (0.9.10-1) unstable; urgency=medium

  * New upstream version
  * Fix version string DESCRIPTION

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jan 2021 07:44:57 +0100

r-cran-luminescence (0.9.8-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 23 Nov 2020 14:06:27 +0100

r-cran-luminescence (0.9.7-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 13 Jan 2020 10:49:40 +0100

r-cran-luminescence (0.9.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.4.1

 -- Dylan Aïssi <daissi@debian.org>  Sat, 04 Jan 2020 09:48:32 +0100

r-cran-luminescence (0.9.5-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Archive, Bug-Database.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- Andreas Tille <tille@debian.org>  Mon, 23 Sep 2019 13:47:36 +0200

r-cran-luminescence (0.9.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Set upstream metadata fields: Contact, Name.

 -- Andreas Tille <tille@debian.org>  Fri, 02 Aug 2019 11:25:46 +0200

r-cran-luminescence (0.9.0.110-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/copyright
  * Build-Depends: r-cran-deoptim
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Wed, 17 Jul 2019 08:50:52 +0200

r-cran-luminescence (0.8.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Sat, 06 Oct 2018 14:28:22 +0200

r-cran-luminescence (0.8.5-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 10 Jun 2018 08:27:16 +0200

r-cran-luminescence (0.8.4-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jun 2018 15:27:44 +0200

r-cran-luminescence (0.8.4-1) unstable; urgency=medium

  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Mon, 23 Apr 2018 08:41:03 +0200

r-cran-luminescence (0.8.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3
  * debhelper 11
  * New Build-Depends: r-cran-plotrix
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Tue, 13 Mar 2018 12:37:04 +0100

r-cran-luminescence (0.7.5-1) unstable; urgency=medium

  * New upstream version
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * debhelper 10
  * Build-Depends: r-cran-magrittr
  * Testsuite: autopkgtest-pkg-r
  * Add debian/README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Thu, 12 Oct 2017 17:18:20 +0200

r-cran-luminescence (0.6.4-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Canonical homepage for CRAN
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Fri, 11 Nov 2016 09:05:30 +0100

r-cran-luminescence (0.6.1-1) unstable; urgency=medium

  * New upstream version
  * Versioned (Build-)Depends from r-cran-rcpp (>= 0.12.5)

 -- Andreas Tille <tille@debian.org>  Wed, 13 Jul 2016 17:03:27 +0200

r-cran-luminescence (0.6.0-1) unstable; urgency=low

  * Initial release (closes: #829665)

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jul 2016 09:16:39 +0200
